const twirlLoadingAnimationWithMessage = (message) => {
  const twirlPositions = ["\\", "|", "/", "-"];
  let x = 0;
  return setInterval(() => {
    process.stdout.write("\r" + `${message}... ${twirlPositions[x++]}`.cyan);
    x &= 3;
  }, 250);
};

module.exports = {
  twirlLoadingAnimationWithMessage,
};
