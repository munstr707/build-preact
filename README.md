# build-basic-preact

## Description

Basic Preact project generator with everything you need to get started with a basic project including Preact and Preact-Compat for compatability with React libraries, css modules, asset loading, transpiling, bundling and more out of the box!

## Status and Future

Currently several important features are missing from the generator out of the box and that includes convenient code splitting. May or may not be added in time.

More importantly this is not available quite yet on npm meaning the steps below are instructions for how to use it in spite of this. An NPM package and further instructions for it are comeing.

## Usage

Non-npm installation

1.  Clone repository
2.  `npm i` Install Dependencies
3.  `npm i -g` Globally link
4.  Run `build-basic-preact` in desired directory for project
