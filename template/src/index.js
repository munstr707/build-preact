import { h, render } from "preact";

let root;

const init = () => {
  let App = require("./components/app").default;
  root = render(<App />, document.body, root);
};

if (module.hot) {
  require("preact/devtools");
  module.hot.accept("./components/app", init);
}

init();
