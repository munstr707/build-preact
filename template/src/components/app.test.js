import { h } from "preact";
import { expect } from "chai";

import App from "./app.js";

describe("Index", () => {
  it("should render a button to the user", () => {
    const testComponent = <App />;
    expect(testComponent).to.contain(<div>test</div>);
    expect(testComponent).to.contain(<button>Click Me!</button>);
  });
});
