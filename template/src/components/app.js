import { h } from "preact";
import "../style/app.css";
import imageURL from "../code.png";

const App = () => (
  <div id="foo">
    <img src={imageURL} alt="tests image" />
    <div>test</div>
    <button onClick={() => alert(":D")}>Click Me!</button>
  </div>
);

export default App;
